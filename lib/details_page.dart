import 'package:cat_apps/breed_model.dart';
import 'package:cat_apps/home_page.dart';
import 'package:flutter/material.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key, required this.breedModel}) : super(key: key);

  final BreedModel breedModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(breedModel.name ?? 'N/A'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          //
          // Item 1 - Picture
          GetCatImageWidget(breedModel: breedModel, height: 400),

          //
          // Item 2 - Details
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //
                // Breed Name
                Text(
                  breedModel.name ?? 'N/A',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),

                //
                // Origin
                Text(
                  breedModel.origin ?? 'N/A',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    color: Colors.grey,
                  ),
                ),

                //
                //
                SizedBox(height: 10),

                //
                // Total lifespan
                Text(
                  'Total Lifespan: ' + (breedModel.life_span ?? 'N/A'),
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),

                //
                //
                SizedBox(height: 10),

                //
                // Hairless cat?
                Text(
                  'Is the cat hairless: ' + (breedModel.hairless == 0 ? 'No' : 'Yes'),
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
