import 'dart:convert';
import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
// import 'package:sook_v1/utils/app/endpoint.dart';
// import 'package:sook_v1/utils/networking/api_util.dart';

Future<UserCredential?> signIn({
  // required String fcmToken,
  required String email,
  required String password,
}) async {
  try {
    final _userCredential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);

    final _user = _userCredential.user;

    final _idToken = await _user!.getIdToken();

    return _userCredential;
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      print('No user found for that email.');
      throw Exception('No user found for that email.');
    } else if (e.code == 'wrong-password') {
      print('Wrong password provided for that user.');
      throw Exception('Wrong password provided for that user.');
    }
  }
}

Future<UserCredential?> registerUsingEmail({
  required String email,
  required String password,
}) async {
  try {
    final _userCredential = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);

    return _userCredential;
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      print('The password provided is too weak.');
      throw Exception('The account already exists for that email.');
    } else if (e.code == 'email-already-in-use') {
      print('The account already exists for that email.');
      throw Exception('The account already exists for that email.');
    }
  } catch (e) {
    print('Wrong password provided for that user.');
    throw Exception('Wrong password provided for that user.');
  }
}

Future<bool?> logOut() async {
  try {
    await FirebaseAuth.instance.signOut();

    return true;
  } on FirebaseAuthException catch (e) {
    throw Exception(e.toString());
  } catch (e) {
    throw Exception(e.toString());
  }
}
