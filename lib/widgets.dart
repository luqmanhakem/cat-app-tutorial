import 'package:flutter/material.dart';

class InputButton extends StatelessWidget {
  InputButton(
      {Key? key,
      required this.onPressed,
      required this.title,
      required this.color})
      : super(key: key);

  final GestureTapCallback onPressed;
  final String title;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        title,
        style: TextStyle(color: color),
      ),
    );
  }
}

class InputTextIcon extends StatelessWidget {
  InputTextIcon({
    Key? key,
    required this.onSubmitted,
    required this.hintText,
    required this.color,
    required this.fontColor,
  }) : super(key: key);

  final Function(String) onSubmitted;
  final String hintText;
  final Color color;
  final Color fontColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(20)),
      child: TextField(
        textAlign: TextAlign.start,
        onSubmitted: onSubmitted,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.all(10),
            child: Icon(Icons.search),
          ),
          contentPadding: const EdgeInsets.only(top: 16),
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: hintText,
        ),
      ),
    );
  }
}
