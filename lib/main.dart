import 'package:cat_apps/login_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  // init widget
  WidgetsFlutterBinding.ensureInitialized();
  // init Firebase
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}

class FirstPage extends StatefulWidget {
  // Constructor
  FirstPage({Key? key}) : super(key: key);

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  String containerTitle = 'Input';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: AppBar(
        title: Text('First Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //
            //
            InputButton(
              title: 'Button 1',
              onPressed: () {
                print('Print Button 1');
              },
            ),
            //
            //
            InputButton(
              title: 'Button 2',
              onPressed: () {
                print('Print Button 2');
              },
            ),
            //
            //
            InputButton(
              title: 'Button 3',
              onPressed: () {
                print('Print Button 3');
              },
            ),
            //
            //
            InputButton(
              title: 'Button 4',
              onPressed: () {
                print('Print Button 4');
              },
            )
          ],
        ),
      ),
    );
  }

  Future<String> login() async {
    String userName = 'Ali@gmail.com';

    await Future.delayed(Duration(seconds: 2));

    return userName;
  }
}

class InputButton extends StatelessWidget {
  InputButton({Key? key, required this.onPressed, required this.title}) : super(key: key);

  final GestureTapCallback onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(title),
    );
  }
}
