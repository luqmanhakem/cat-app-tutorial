import 'package:cat_apps/home_page.dart';
import 'package:cat_apps/main.dart';
import 'package:cat_apps/register_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InputText(
              title: 'Email',
              onChanged: (String value) {
                emailController.text = value;
              },
            ),
            SizedBox(height: 20),
            InputText(
              title: 'Password',
              isHide: true,
              onChanged: (String value) {
                passwordController.text = value;
              },
            ),
            SizedBox(height: 20),
            InputButton(
              onPressed: () async {
                if (emailController.text.isEmpty ||
                    passwordController.text.isEmpty) {
                  await showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text('Oops'),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Please fill all the field'),
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Okay'),
                              )
                            ],
                          ),
                        );
                      });
                  return;
                } else {
                  final userCrendential =
                      await FirebaseAuth.instance.signInWithEmailAndPassword(
                    email: emailController.text,
                    password: passwordController.text,
                  );

                  if (userCrendential.user != null) {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  } else {
                    await showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Oops'),
                            content: Text('User Not Found'),
                          );
                        });
                  }
                }
              },
              title: 'Login',
            ),

            InputButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage()));
            }, title: 'Register now')
          ],
        ),
      ),
    );
  }
}

class InputText extends StatelessWidget {
  InputText({
    Key? key,
    required this.title,
    required this.onChanged,
    this.isHide = false,
  }) : super(key: key);

  final String title;
  final Function(String) onChanged;
  final bool isHide;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(title),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: TextFormField(
            onChanged: onChanged,
            obscureText: isHide,
          ),
        )
      ],
    );
  }
}
