class BreedModel {
  final String? id;
  final String? name;
  final String? temperament;
  final String? life_span;
  final String? alt_names;
  final String? wikipedia_url;
  final String? origin;
  final String? weight_imperial;
  final int? hairless;
  final String? reference_image_id;

  BreedModel({
    this.id,
    this.name,
    this.temperament,
    this.life_span,
    this.alt_names,
    this.wikipedia_url,
    this.origin,
    this.weight_imperial,
    this.hairless,
    this.reference_image_id,
  });

  factory BreedModel.fromJson(Map<String, dynamic> json) {
    return BreedModel(
      id: json['id'],
      name: json['name'],
      temperament: json['temperament'],
      life_span: json['life_span'],
      alt_names: json['alt_names'],
      wikipedia_url: json['wikipedia_url'],
      origin: json['origin'],
      weight_imperial: json['weight_imperial'],
      hairless: json['hairless'],
      reference_image_id: json['reference_image_id'],
    );
  }
}
