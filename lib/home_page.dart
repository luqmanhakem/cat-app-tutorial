import 'dart:convert';
import 'dart:io';

import 'package:cat_apps/breed_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'details_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<BreedModel> breedList = [];

  Future<List<BreedModel>> searchBreed(String breedName) async {
    final response = await http.get(
        Uri.parse('https://api.thecatapi.com/v1/breeds/search?q=$breedName'),
        headers: {
          HttpHeaders.authorizationHeader:
              '6217a045-2b15-48ea-b9cf-8e30a1f47ea5',
        });

    if (response.statusCode == 200) {
      // Success
      final list = List<BreedModel>.from(
        json.decode(response.body).map(
          (data) {
            return BreedModel.fromJson(data);
          },
        ),
      ).toList();

      return list;
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Drawer Header'),
            ),
            ListTile(
              title: Text('Item 1'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: Text('Item 2'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          children: [
            Text('Location', style: TextStyle(fontSize: 12)),
            Text('Selangor, Malaysia', style: TextStyle(fontSize: 16))
          ],
        ),
      ),
      body: Column(
        children: [
          //
          // Item 1
          SizedBox(height: 10),
          //
          // Item 2
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: TextField(
                onSubmitted: (value) async {
                  print(value);
                  final list = await searchBreed(value);

                  setState(() {
                    breedList = list;
                  });
                },
                decoration: InputDecoration(
                  hintText: 'Search cat breed',
                  prefixIcon: Icon(Icons.search),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
              ),
            ),
          ),
          //
          // Item 3
          SizedBox(height: 10),
          //
          // Item 4 - ListView.Builder
          Expanded(
            child: ListView.builder(
              itemCount: breedList.length,
              itemBuilder: (context, index) {
                return _CatPreviewCard(
                  breedList: breedList,
                  index: index,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _CatPreviewCard extends StatelessWidget {
  _CatPreviewCard({Key? key, required this.breedList, required this.index})
      : super(key: key);

  final List<BreedModel> breedList;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailsPage(
                          breedModel: breedList[index],
                        )));
          },
          child: Container(
            child: Row(
              children: [
                //
                // Item 1 in Row - Image
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    child: GetCatImageWidget(
                      breedModel: breedList[index],
                    ),
                  ),
                ),

                // Item 2 in Row - Details
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    child: Container(
                      height: 200,
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //
                            // Item 1 - Breed Name
                            Text(breedList[index].name ?? 'N/A',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),

                            //
                            // Item 2 - Origin
                            Text(breedList[index].origin ?? 'N/A',
                                style: TextStyle(
                                    fontSize: 14, color: Colors.grey)),

                            //
                            // Item 3 - Desc
                            SizedBox(height: 15),

                            //
                            // Item 4 - Desc
                            Text(breedList[index].temperament ?? 'N/A',
                                style: TextStyle(fontSize: 13)),

                            //
                            // Item 5 - Space/Ruangan
                            Spacer(),

                            //
                            // Item 6- See More Text
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Text('See More >',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.grey)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class GetCatImageWidget extends StatelessWidget {
  const GetCatImageWidget({
    Key? key,
    required this.breedModel,
    this.height = 200,
  }) : super(key: key);

  final BreedModel breedModel;
  final double height;

  Future<String> getImage(String imageId) async {
    final response = await http.get(
        Uri.parse('https://api.thecatapi.com/v1/images/$imageId'),
        headers: {
          HttpHeaders.authorizationHeader:
              '6217a045-2b15-48ea-b9cf-8e30a1f47ea5',
        });

    if (response.statusCode == 200) {
      // Success
      final image = jsonDecode(response.body) as Map<String, dynamic>;

      return image['url'];
    } else {
      return 'https://www.mcicon.com/wp-content/uploads/2021/03/Cat-07-300x300.jpg';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: getImage(breedModel.reference_image_id ?? ''),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: height,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: Image.network(snapshot.data!, fit: BoxFit.cover),
          );
        } else {
          return Image.network(
              'https://www.mcicon.com/wp-content/uploads/2021/03/Cat-07-300x300.jpg',
              fit: BoxFit.cover);
        }
      },
    );
  }
}
